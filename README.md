### Movie React

> Simple React app using Context API to add favourites and render lists

![screenshot](/public/movie.gif)

### How to install

1. `git clone`

2. `npm install`

3. `npm start`


### What I've learnt

* Using context api to lift state from component level to a global overarching state
* Used localstorage to persist state even when the browser window is closed