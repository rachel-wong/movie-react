// describes how the state is transferred into the next state
// modifies the state here when triggered
// action tells how to change the state

export default (state, action) => {
  switch (action.type) {

    // modify just the watchlist component of the global state, and just add the action.payload tto taht part
    // modifying the whole state by modifying just a small part of that whole state
    case "ADD_MOVIE_TO_WATCHLIST":
      return {
        ...state,
        watchlist: [action.payload, ...state.watchlist]
      }
    case "REMOVE_MOVIE_FROM_WATCHLIST":
      return {
        ...state,
        watchlist: state.watchlist.filter(item => item.id !== action.payload)
      }
    case "ADD_MOVIE_TO_WATCHED":
      return {
        ...state,
        watched: [action.payload, ...state.watched],
        watchlist: state.watchlist.filter(item => item.id !== action.payload.id)
      }
    case "MOVE_TO_WATCHLIST":
      return {
        ...state,
        watched: state.watched.filter(
          (movie) => movie.id !== action.payload.id
        ),
        watchlist: [action.payload, ...state.watchlist],
      }
    case "REMOVE_FROM_WATCHED":
      return {
        ...state,
        watched: state.watched.filter(item => item.id !== action.payload)
      }
    default:
      return state; // return the state unchanged
  }
}