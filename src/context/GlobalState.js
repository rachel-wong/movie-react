import { createContext, useReducer, useEffect } from "react"
import AppReducer from './AppReducer'

// shape of the initial value of the store
// check if there's anything in localstorage or it'ss a blank array so that on app load there could be items loaded on the watchlist and not always blank every single time it is loaded
const initialState = {
  watchlist: localStorage.getItem('watchlist') ? JSON.parse(localStorage.getItem('watchlist')) : [],
  watched: localStorage.getItem('watched') ? JSON.parse(localStorage.getItem('watched')) : [],
}

// create global context for the app
export const GlobalContext = createContext(initialState)

// global context needs to be given to the global provider which will then "provide " it to the whole application
export const GlobalProvider = ({ children }) => {

  // define the state and dispatcher to allow the global state to be changed
  const [state, dispatch] = useReducer(AppReducer, initialState)
  // define actions that would change the state

  // triggered whenever the state is changed inside the provider (item added or removed)
  useEffect(() => { 
    localStorage.setItem('watchlist', JSON.stringify(state.watchlist))
    localStorage.setItem('watched', JSON.stringify(state.watched))
  }, [state])

  // actions to tell the reducer what to do the state
  // any actions ç here need to be passed to the Global Provider in order to mak eit avialable to the whole app
  const addMovieToWatchlist = movie => {
    // dispathc a type to tell the reducer what to do with the state
    // this calls the reducer
    dispatch({ type: "ADD_MOVIE_TO_WATCHLIST", payload: movie })
  }

  // remove movie from watchlist
  const removeMovieFromWatchlist = id => {
    dispatch({type: "REMOVE_MOVIE_FROM_WATCHLIST", payload: id })
  }

  // move move from watchlist to watched
  const addMovieToWatched = movie => {
    dispatch({ type: "ADD_MOVIE_TO_WATCHED", payload: movie })
  }

  // move from watched to watchlist
  const moveToWatchlist = movie => {
    dispatch({type: "MOVE_TO_WATCHLIST", payload: movie})
  }

  const removeFromWatched = id => {
    dispatch({ type: "REMOVE_FROM_WATCHED", payload: id})
  }

  return (
    // provide specific parts of the state that the app should be able to access
    <GlobalContext.Provider value={{removeFromWatched, moveToWatchlist, addMovieToWatched, removeMovieFromWatchlist, addMovieToWatchlist, watchlist: state.watchlist, watched: state.watched}}>
      { children }
    </GlobalContext.Provider>
  )
}
