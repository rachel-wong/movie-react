import './App.css';
import { BrowserRouter as Router, Link, Switch, Route } from 'react-router-dom';
import Header from './components/Header'
import Add from './components/Add'
import Watchlist from './components/Watchlist'
import Watched from './components/Watched'
import { GlobalProvider } from './context/GlobalState'
function App() {
  return (
    <GlobalProvider>
      <Router>
        <Header />

        <Switch>
          <Route exact path="/">
            <Watchlist />
          </Route>
          <Route path="/add">
            <Add />
          </Route>
          <Route path="/watched">
            <Watched />
          </Route>
        </Switch>
      </Router>
    </GlobalProvider>
  );
}

export default App;
