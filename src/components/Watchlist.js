import React, { useContext } from 'react'
import { GlobalContext} from '../context/GlobalState'
import MovieCard from '../components/MovieCard'

const Watchlist = () => {

  // get watchlist from the global context to display here
  const { watchlist } = useContext(GlobalContext)

  return (
    <div className="movie-page">
      <div className="container">
        <div className="header">
          <h1 className="heading">My watchlist </h1>
          <span className="count-pill">{watchlist.length} { watchlist.length > 1 ? "movies" : "movie"}</span>
        </div>

        {/* identifies to the moviecard the movie is already in the watchlist */}
          <div className="movie-grid">
          {watchlist.length > 0 ? watchlist.map((movie, idx) => (
              <MovieCard key={idx} movie={movie} type="watchlist" />
            )) :
            <h2 className="no-movies">No movies in your list.</h2>
          }
        </div>

      </div>
    </div>
  )
}

export default Watchlist
