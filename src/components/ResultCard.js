import React, { useContext } from 'react'
import { GlobalContext } from '../context/GlobalState'

const ResultCard = ({ movie }) => {

  const { addMovieToWatchlist, watchlist, watched, addMovieToWatched } = useContext(GlobalContext);

  // prevent movie being added more than once when
  // search watchlist store to see if the movie is in there
  let storedMovie = watchlist.find((o) => o.id === movie.id);
  let storedWatched = watched.find((o) => o.id === movie.id);

  // // if there is a storedmovie, turn off the <button>
  // // the disabled attribute at the <button> does the job of "disabling" the button
  // is it in the watchlist? and if not, is it in the watched ?
  const watchlistDisabled = storedMovie ? true : storedWatched ? true: false; // keep separate from add to watchlist button

  const watchDisabled = storedWatched ? true : false;

  return (
    <div className="result-card">
      <div className="poster-wrapper">
        {movie.poster_path ? (
          <img src={`https://image.tmdb.org/t/p/w200${movie.poster_path}`} alt={ movie.title + " poster" } />
        ) : (
            <div className="filler-poster"></div>
        )}
      </div>
      <div className="info">
        <div className="header">
          <h3 className="title">{movie.title}</h3>
          <h4 className="release-date">
            { movie.release_date != null ? movie.release_date.substring(0, 4) : "-"}
          </h4>
        </div>
        <div className="controls">
          <button disabled={ watchlistDisabled} className="btn" onClick={ () => addMovieToWatchlist(movie)}>Add to Watchlist</button>
          <button disabled={ watchDisabled } className="btn" onClick={ () => addMovieToWatched(movie)}>Add movie to watched</button>
        </div>
    </div>
      </div>
  )
}

export default ResultCard