import React from 'react'
import { useContext} from "react"
import { GlobalContext } from '../context/GlobalState'
import MovieCard from './MovieCard'

const Watched = () => {

  const { watched } = useContext(GlobalContext)

  return (
  <div className="movie-page">
        <div className="container">
          <div className="header">
          <h1 className="heading">Watched Movies </h1>
          <span className="count-pill">{watched.length} { watched.length > 1 ? "movies" : "movie"}</span>
          </div>

          {/* identifies to the moviecard the movie is already in the watchlist */}
            <div className="movie-grid">
            {watched.length > 0 ? watched.map((movie, idx) => (
                <MovieCard key={idx} movie={movie} type="watched" />
              )) :
              <h2 className="no-movies">No movies in your list.</h2>
            }
          </div>

        </div>
    </div>
  )
}

export default Watched
