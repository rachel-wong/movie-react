import React, { useState} from 'react'
import ResultCard from '../components/ResultCard'
// search movie db and fetch results
const Add = () => {
  const [query, setQuery] = useState("")
  const [results, setResults] = useState([])

  const onChange = (e) => {
    e.preventDefault();
    setQuery(e.target.value);

    // console.log("env", process.env.REACT_APP_API_KEY)
    // fetch data from
    fetch(`https://api.themoviedb.org/3/search/movie?api_key=${process.env.REACT_APP_API_KEY}&language=en-US&page=1&include_adult=falses&query=${query}`)
      .then(res => res.json())
      .then(res => {
        if (!res.errors) {
          setResults(res.results)
        } else {
          setResults([])
          console.error(res.errors)
        }
      })
  }
  return (
    <div className="add-page">
      <div className="container">
        <div className="add-content">
          <div className="input-wrapper">
            <input type="text"
              value={query}
              onChange={onChange }
              placeholder="Search for movies" />
          </div>
          {results.length > 0 && (
            <ul className="results">
              {results.map((movie, idx) => (
                <li>
                  <ResultCard key={idx} movie={ movie }/>
                </li>
              ))}
            </ul>
          )}
        </div>
      </div>
    </div>
  )
}

export default Add
